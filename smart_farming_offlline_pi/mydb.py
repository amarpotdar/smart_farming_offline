import mysql.connector
import json
import constant

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="root",
    database="mydatabase"
)

print(mydb)

mycursor = mydb.cursor()


def printLog(msg):
    print("\ndatabase :----> " + str(msg))


def initdbconnection():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="root",
        database="mydatabase"
    )

    printLog(mydb)

    mycursor = mydb.cursor()

    try:
        mycursor.execute("CREATE DATABASE mydatabase")
        printLog("DB created successfully..")
    except:
        printLog("mydatabse database is alerady created...")
    mycursor.execute("SHOW DATABASES")
    for x in mycursor:
        printLog(x)
    createtables(mycursor)


def createtables(mycursor):
    try:
        mycursor.execute(
            "CREATE TABLE schedule (id VARCHAR(191) PRIMARY KEY,data VARCHAR(2617))")
        printLog("schedule TABLE successfully..")
    except Exception as e:
        printLog(
            "------------------------> schedule TABLE is alerady created. <------------------------")
        printLog(e)

    try:
        mycursor.execute(
            "CREATE TABLE offline (id INT PRIMARY KEY AUTO_INCREMENT,data VARCHAR(2617))")
        printLog("offline TABLE successfully..")
    except Exception as e:
        printLog(
            "------------------------> offline TABLE is alerady created. <------------------------")
        printLog(e)

    try:
        mycursor.execute(
            "CREATE TABLE sensor (id INT PRIMARY KEY AUTO_INCREMENT,data VARCHAR(2617))")
        printLog("sensor TABLE successfully..")
    except Exception as e:
        printLog(
            "------------------------> sensor TABLE is alerady created. <------------------------")
        printLog(e)

    mycursor.execute("SHOW TABLES")
    for x in mycursor:
        printLog(x)
    printLog(updateSchedule())
    printLog(getOfflineData())
    printLog(getSensorData())


def getdatafrom(table_name):
    sql = "SELECT * FROM "+table_name
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    return myresult


def updateSchedule():
    constant.setSchedule(getdatafrom('schedule'))
    return constant.getSchedule()


def clearData():
    try:
        mycursor.execute("TRUNCATE schedule")
        printLog("schedule data cleared successfully........................!")
    except:
        printLog(
            "------------------------> Exception in TRUNCATE schedule <------------------------")


def clearOfflineData():
    try:
        mycursor.execute("TRUNCATE offline")
        printLog("offline data cleared successfully........................!")
    except:
        printLog(
            "------------------------> Exception in offline schedule <------------------------")


def clearSensorData():
    try:
        mycursor.execute("TRUNCATE sensor")
        printLog("sensor data cleared successfully........................!")
    except:
        printLog(
            "------------------------> Exception in sensor schedule <------------------------")


def syncSchedule(id, schedule):
    try:
        sql = "REPLACE INTO schedule (id,data) VALUES (%s,%s)"
        val = (str(id), str(schedule))
        mycursor.execute(sql, val)
        mydb.commit()
        printLog("Schedule sync Record inserted/updated")
    except Exception as e:
        printLog(
            "\n ------------------------> Exception in syncSchedule function <------------------------")
        printLog(e)


def deleteSchedule(id):
    try:
        sql = "DELETE FROM schedule WHERE id = %s"
        val = (str(id),)
        mycursor.execute(sql, val)
        mydb.commit()
        printLog("Schedule Record deleted")
    except Exception as e:
        printLog(
            "\n ------------------------> Exception in deletSchedule function <------------------------")
        printLog(e)


def getOfflineData():
    printLog("get offline data")
    logs = getdatafrom("offline")
    result = []
    for data in logs:
        try:
            result += json.loads(data[1])
        except Exception as e:
            printLog("\nException in getOfflineData function **")
            printLog(e)
    return json.dumps(result)


def getSensorData():
    printLog("get sensor data")
    logs = getdatafrom("sensor")
    result = []
    for data in logs:
        try:
            result.append(json.loads(data[1]))
        except Exception as e:
            printLog("\nException in getOfflineData function **")
            printLog(e)
    return json.dumps(result)


def logOfflineData(data):
    printLog("-------------------------------------logOfflineData----------------------------------------")
    printLog(data)
    try:
        sql = "INSERT INTO offline (data) VALUES (%s)"
        val = (data,)
        mycursor.execute(sql, val)
        mydb.commit()
        printLog("record inserted.")
        return 1
    except Exception as e:
        printLog(e)
        printLog("logOfflineData ********** something went wrong ************")
        return 0


def logSensorData(data):
    printLog("-------------------------------------logSensorData----------------------------------------")
    printLog(data)
    try:
        sql = "INSERT INTO sensor (data) VALUES (%s)"
        val = (data,)
        mycursor.execute(sql, val)
        mydb.commit()
        printLog("record inserted.")
        return 1
    except Exception as e:
        printLog(e)
        printLog("logSensorData ********** something went wrong ************")
        return 0
