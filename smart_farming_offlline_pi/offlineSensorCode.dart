
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <BH1750.h>
#include <SoftwareSerial.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Arduino.h>
#include "Adafruit_SHT31.h"
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoMqttClient.h>
#include <ArduinoJson.h>


extern "C"{
#include <lwip/icmp.h>
}

#define SERIAL_BAUD 9600

BH1750 lightMeter;
WiFiClient client;
WiFiClient wiFiClient;
MqttClient mqttClient(client);
Adafruit_BME280 bme;
Adafruit_SHT31 sht31 = Adafruit_SHT31();

WiFiServer server(80);
int status_wifi = WL_IDLE_STATUS;
WiFiManager wifiManager;

char vyaprut[] = "www.vyaprut.com";
int vyaprut_port = 80;

String device_id = "X66";

const char broker[] = "192.168.1.111";
int        port     = 1883;
const char topic[]  = "arduino_producer";

bool ischeckdata = true;//True for Automation False for monitering

bool isBME = true ;//if BME280 is used in circuit True else false

bool isSHT = false;//if SHT30 is used in circuit True else false

int flag = 0; // To ignore first reading of all sensors.
int wifi_cnt = 0;
int cnt = 567;      // this count is used for timeinterval.init value
int cntLimit = 543; //This is max limit of timeinterval.543
int check_cnt = 20;//init value
int bmeCheckCount = 0; //
int bmeReady = 0;      // to check BEM280 is ready to run or not
int bhtCheckCount = 0;
int bhtReady = 0; //to Check is BHT175 ready to run or not
int ledPin = 13; // LED light network connection info

void setup(){
  Serial.begin(SERIAL_BAUD); //serial moniter
  while(!Serial){ }

  Serial.println("in set up");

  pinMode(ledPin, OUTPUT);
 
  delay(1000);

  digitalWrite(ledPin, LOW);
  
  Wire.begin();
  String hotspot = "Sensor_"+device_id;
  int str_len = hotspot.length() + 1; 
  char hotspotchar[str_len];
  hotspot.toCharArray(hotspotchar, str_len);
  Serial.println("connecting to wifi");
  
  wifiManager.autoConnect(hotspotchar);
  
//boolean result = WiFi.softAP(device_id + "_ACT", "akat1211");// hotspot on

  server.begin();

  Serial.print("Attempting to connect to the MQTT broker: ");
  Serial.println(broker);
  ledLightOn(true);
  delay(1000);
  ledLightOn(false);
  delay(1000);  
  setupBHT175();
  Serial.println("light is set up");
  delay(1000);
  if(isBME){
  setupBME280();
  }else if(isSHT){
      setupSHT();
    }
  Serial.println("Temp ,Hum is set up");
  delay(1000);
  
}


void loop() {  
    while (WiFi.status() == WL_CONNECTED){
        wifi_cnt = 0;
        Serial.println("connected to wifi");
        ledLightOn(true);
        cnt = cnt + 1;
        check_cnt = check_cnt + 1;
        Serial.println(cnt);
        if(ischeckdata){
          if(check_cnt > 15){
              Serial.println("inside check_cnt : true");
              ShowSerialData("checkdata");
              cnt = cnt + 1;
           }
        }
        if(cnt > cntLimit){
            ShowSerialData("postdata");
        }
        delay(1000);
    }
    
    Serial.println("Wifi disconnected");
    Serial.println(wifi_cnt);
    ledLightOn(false);
    
    delay(2000);
    
    if(wifi_cnt>20){
        ESP.reset();
    }
    cnt = cnt + 1;
    wifi_cnt = wifi_cnt + 1;
}

void ledLightOn(bool status){
    if(status){
      Serial.println("LED light : ON");
      digitalWrite(ledPin, HIGH);
    }else{
      Serial.println("LED light : OFF");
      digitalWrite(ledPin, LOW);
      }
  }



void ShowSerialData(String modeVariable){
  Serial.println("inside ShowSerialData  modeVariable: ");
  Serial.print(modeVariable);
  Serial.println("\n");
  float temperature = 99;
  float humidity = -1;
  uint16_t lux = 99999;
  uint16_t soil = 0;
   uint16_t lsoil = 111;
  digitalWrite(6,HIGH);
  delay(2000);
  getLuxData(lux);
  if(isBME){
      getBME280Data(&Serial,temperature,humidity);
  }else if(isSHT){
    getSHTData(&Serial,temperature,humidity);
  }
  Serial.print("Temperature \t");
  Serial.println(temperature, 1);
  Serial.print("humidity \t");
  Serial.println(humidity, 1);
  if(isnan(temperature) || isnan(humidity)){
    Serial.println("temperature or humidity NOT A NUMBER");
    ShowSerialData(modeVariable);
    }
  else{
       if (flag != 0) {
          if(modeVariable.equals("checkdata"))
            {
              sendData(temperature,humidity,lux,soil,"checkdata",0); 
            }else{
              sendData(temperature,humidity,lux,lsoil,"postdata",1);
            }
        }
       if (flag != 1) {
           Serial.println("inside flag != 1");
           flag = 1;
           delay(9000);
           ShowSerialData(modeVariable);
        }
    }
  
}

// Function to setup BHT175 


void setupBHT175(){

  Wire.begin();
 // lightMeter.begin();
  if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
    Serial.println(F("LIGHT Advanced begin"));
    bhtReady = 1;
  }
  else {
    Serial.println(F("Error initialising Light Sensor"));
    bhtCheckCount = bhtCheckCount + 1;
    delay(3000);
    if(bhtCheckCount < 5){
      setupBHT175();  
     }
    
    }
  }

// Funtion to setup BME280 
 
void setupBME280(){
   Serial.println(F("T-H-P sensor test"));
   bool status; 
   status = bme.begin();  
   if(!status) {
        Serial.println("Could not find a valid T-H-P sensor, check wiring!");
       // while (1);
       bmeCheckCount = bmeCheckCount + 1;
       //digitalWrite(6,HIGH);
       delay(3000);
       if(bmeCheckCount < 5){
         setupBME280();
        }
       
    }else{
      Serial.println("Sensor is ready to run.......");
      bmeReady = 1;
      
    }
  }

void setupSHT(){
   Serial.println(F("T-H-P 30 test"));
   bool status; 
   status = sht31.begin(0x44); 
   if(!status) {
        Serial.println("Could not find a valid T-H-P 30 sensor, check wiring!");
       bmeCheckCount = bmeCheckCount + 1;
       delay(3000);
       if(bmeCheckCount < 5){
         setupSHT();
        }
    }else{
      Serial.println("T-H-P 30 sensor is ready to run.......");
      bmeReady = 1;
      Serial.print("Heater Enabled State: ");
      if (sht31.isHeaterEnabled())
        Serial.println("ENABLED");
      else
        Serial.println("DISABLED");
    }
  }
  
// Function to get temperature and humdity from BME280

void getBME280Data(Stream* client,float &temp,float &humi)
{
   temp = bme.readTemperature();
   humi = bme.readHumidity();
}

void getSHTData(Stream* client,float &temp,float &humi)
{
   temp = sht31.readTemperature();
   humi = sht31.readHumidity();
}

// Function to get Lux from BHT175

void getLuxData(uint16_t &luxVal){
  luxVal = lightMeter.readLightLevel();
  Serial.print("Light: ");
  Serial.println(luxVal);
  luxVal = luxVal + ((luxVal/100)*40);
  Serial.print("Light after adjustment : ");
  Serial.println(luxVal);
}

void sendData(int temp, int hum, int lux, int soil, String modeVariable,int sflag) {

   if(temp<5 || hum<0 || lux<0){
      ESP.reset();
    }  
   String tempString = String(temp);
   String humString = String(hum);
   String luxString = String(lux);
   String soilString = String(soil);
   String statusString = String(sflag);
   String json = "{\"device_id\":\""+ device_id +"\",\"status\":\"" + statusString+"\",\"temp\":\"" + tempString +"\",\"hum\":\""+humString+"\",\"co2\":\"" + soilString +"\",\"lux\":\""+ luxString + "\"}";
    
    Serial.println(json);
    if(modeVariable == "checkdata"){
          sendMessageToController(json);
          check_cnt = 0;
     }else{
        cnt = 0;
        int j = 0;
        int max_limit = 10;
        while(!wiFiClient.connect(vyaprut, vyaprut_port)){
        j = j + 1;
        if (j > max_limit) {
          break;
        }
        Serial.print("Not connected to www.vyaprut.com Try :");
        Serial.println(j);
        delay(500);
        }
      
        if (wiFiClient.connect(vyaprut, vyaprut_port)) {
               postData(json);
        }else{
          Serial.println("Not connected to www.vyaprut.com");
          Serial.println("IP address: ");
          Serial.println(WiFi.localIP());
          sendMessageToController(json);
        }
      
        while (wiFiClient.available()) {
          char c = wiFiClient.read();
          Serial.write(c);
        }
      
        if (!wiFiClient.connected()) {
          wiFiClient.stop();
        }
      }

 }

void sendMessageToController(String json){
  if (!mqttClient.connect(broker, port)) {
        Serial.print("Controller connection failed! Error code = ");
        Serial.println(mqttClient.connectError());
   }else{
      Serial.println("Sending data to controller");
      mqttClient.poll();
      mqttClient.beginMessage(topic);
      mqttClient.print(json);
      mqttClient.endMessage();
      Serial.println("Data sent to controller successfully");
      Serial.println("\n");
    }
}

void postData(String json){
    Serial.println("connected to www.vyaprut.com");
    wiFiClient.println("POST /api/addseries HTTP/1.1");
    wiFiClient.println("Host: www.vyaprut.com");
    wiFiClient.println("Content-Type: application/json");
    wiFiClient.println("cache-control: no-cache");
    wiFiClient.print("Content-Length: ");
    int thisLength = json.length();
    wiFiClient.println(thisLength);
    wiFiClient.println("Connection: close");
    wiFiClient.println();
    wiFiClient.println(json);
    Serial.println("Data sent to server successfully");
    Serial.println("\n");
}