schedule = []
switchState = [0] * 15
intervalState = [0] * 15
currentState = [0] * 15
otiState  = [0] * 15
isInterval = False
climate = {}
isOnline = False
sio = {}


def getSwitchCount():
    return 5

def getControllerId():
    return "CNT21"

def getServerVersion():
    return "1.0.1"

def getSchedule():
    return schedule

def setSchedule(iSchedule):
    global schedule
    schedule = iSchedule

def getSwitchState():
    return switchState

def setSwitchState(input):
    global switchState
    switchState = input

def getClimate():
    return climate

def setClimate(iClimate):
    global climate
    climate = iClimate

def getIntervalState():
    return intervalState

def setIntervalState(value):
    global intervalState
    intervalState = value

def getIsInterval():
    return isInterval

def setIsInterval(value):
    global isInterval
    isInterval = value

def getIsOnline():
    return isOnline

def setIsOnline(value):
    global isOnline
    isOnline = value

def getSio():
    return sio

def setSio(value):
    global sio
    sio = value

def getCurrentState():
    return currentState

def setCurrentState(value):
    global currentState
    currentState = value

def getOtiState():
    return otiState

def setOtiState(value):
    global otiState
    otiState = value