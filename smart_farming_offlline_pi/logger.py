import os
import logging

currentFolderName = " "
logger = logging.getLogger()
def createDefaultFolder(iObj):
      externalFolderName = "/home/pi/logs/" + iObj["date"]
      currentFolderName = externalFolderName
      try:
        if not os.path.exists(externalFolderName):
            print("inside..not created")
            os.makedirs(externalFolderName)
            print("Done folder created")
        else:
            print("folder is alerady created..")
        loggerSetup()
      except OSError as e:
          print(e)

def loggerSetup():
    fileName = currentFolderName +"file.log"  
    logging.basicConfig(filename=fileName, 
                    format='%(asctime)s %(message)s', 
                    filemode='w')
    logger=logging.getLogger() 
    logger.debug("Harmless debug Message") 
