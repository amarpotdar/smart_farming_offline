print("inside server")
import paho.mqtt.client as mqtt
import minimalmodbus
import json
import time
import socket
import mydb
import run
import socketio
import controller
from utility import writeRegister


not_connected = True
IPAddr = ""
while not_connected :
    try:
        IPAddr = socket.gethostbyname(str(socket.gethostname())+".local")
        print("Server IP Address is:" + IPAddr)
        not_connected = False
    except:
        print("\nConnection :------>Please check your connection")
        time.sleep(3)

client = mqtt.Client(clean_session=True, userdata=None, transport="tcp")
minimalmodbus.CLOSE_PORT_AFTER_EACH_CALL = True
instrument = minimalmodbus.Instrument('/dev/ttyS0', 1) # port name, slave address (in decimal)
instrument.serial.baudrate = 115200   # Baud
instrument.serial.timeout = 1 # seconds
instrument.clear_buffers_before_each_transaction = True
instrument.debug = False

sio = socketio.Client()

MQTT_SERVER = IPAddr
MQTT_PORT = 1883
MQTT_SENDER = "arduino_producer"
MQTT_CONNECTED = "arduino_producer"

print("\n****************** SMART-FARMING Controller started with "+MQTT_SERVER+" ************************\n")

def printLog(msg):
    print("\nSMART-FARMING Controller :----> " + str(msg))


def on_init():
    printLog("Server init Done")
    mydb.initdbconnection()
    printLog("Machine Id "+controller.getId())
    mydb.clearOfflineData()
    mydb.clearSensorData()
    run.initProcess(instrument,mydb)
    
def on_connect(client, userdata, flags, rc):
    printLog("MQTT Connected with result code "+str(rc))
    client.subscribe(MQTT_SENDER)

def on_message(client, userdata, msg):
    printLog("MQTT_on_message ----------------------->")
    if msg.topic == MQTT_CONNECTED:
        try :
            data = json.loads(str(msg.payload.decode("utf-8")))
            if data and data["device_id"]:
                run.updateClimate(instrument,mydb,data)
        except Exception as e:
            printLog("\n ------------------------> Exception in MQTT_on_message function <------------------------")
            printLog(e)

def on_disconnect(client, userdata, rc):
    printLog("Unexpected MQTT disconnection. Will auto-reconnect"+str(rc))

on_init()

client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect
client.enable_logger(logger=None)
client.connect(MQTT_SERVER, MQTT_PORT, 60)

time.sleep(1)

run.controllOffline(client,instrument,time)

while 1:
        printLog("Connecting to server")
        try:
            @sio.event
            def connect():
                sio.emit('request_to_server', {'response': 'i am client....connected'})
                controller.onConnect(instrument,time)
            @sio.event
            def request_to_controller(data):
                printLog("Received data from server :--------------> ")
                printLog(data)
                controller.onDataReceived(sio,instrument,mydb,data)
            @sio.event
            def disconnect():
                controller.offline(instrument)
                # sio.connect('http://192.168.1.108:4001')
                sio.connect('http://www.vyaprut.com')
                sio.wait()
            sio.connect('http://www.vyaprut.com',{ 'query': "cntroller_id="+controller.getId()})
            # sio.connect('http://192.168.1.108:4001',{ 'query': "cntroller_id="+controller.getId()})
            sio.wait()
        except Exception:
            printLog("inside exception while connecting server")
            controller.offline(instrument)
            time.sleep(5)
