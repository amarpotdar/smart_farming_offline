from warnings import catch_warnings
import constant
import json
import array as arr
import scheduler
import datetime
import threading
from utility import writeRegister


def printLog(msg):
    print("\nprocess :----> " + str(msg))


def printInfo(msg):
    print("\nprocess :----> " + str(msg))


def initProcess(instrument,db):
    printLog("Server Version "+constant.getServerVersion())
    constant.setClimate({})
    constant.setSwitchState([0]*15)
    constant.setCurrentState([0]*15)
    constant.setIntervalState([0]*15)
    constant.setIsInterval(False)
    resetSwitches(instrument)
    scheduler.scheduleProcess("run_process", 15,False, instrument, db, runProcess)
    runProcess(instrument,db)
    return 1

def resetSwitches(instrument):
    printLog("Turning off all switches")
    constant.setSwitchState([0] * 10)
    for i in range(1,17):
        writeRegister("interval switch "+str(i),instrument,i,0)
    scheduler.clearAllSchedules()

def runProcess(instrument, param):
    schedules = constant.getSchedule()
    switchState = constant.getSwitchState()
    intervalState = constant.getIntervalState()
    currentState = constant.getCurrentState()
    otiState = constant.getOtiState()
    log = {}
    keys = []
    printLog("schedule length : "+str(len(schedules)))
    isInterval = False
    for data in schedules:
        try :
            schedule = json.loads(data[1])
            switch_register = getRegisterId(schedule)
            if switch_register <= constant.getSwitchCount() :
                state = -1
                printLog(schedule)
                try:
                    if schedule['status'] and int(schedule['status']) == 1:
                        if int(schedule['phase_type']) < 2 :
                            printLog("inside schedule mode block")
                            state = checkSchedule(schedule)
                        elif int(schedule['phase_type']) == 2:
                            printLog("inside manual mode block")
                            state = 1
                        printLog("state of schedule inside try is : "+str(state))
                    else :
                        printLog("Schedule switch is in off mode.")
                        state = 0
                except Exception as e:
                    printLog("\nrouter :------> ** Exception in runProcess checkSchedule function **")
                    printLog(e)
                if(not schedule['device_id'] in log):
                        log[schedule['device_id']] = {}
                if state != -1 and switchState[switch_register] != state:
                    switchState[switch_register] = state
                    currentState[switch_register] = state
                    intervalState[switch_register] = state
                    writeRegister(schedule['switch_id']+" state changed",instrument,switch_register,state)
                    constant.setSwitchState(switchState)
                    constant.setIntervalState(intervalState)
                    constant.setCurrentState(currentState)
                    if(not schedule['device_id'] in keys):
                        keys.append(schedule['device_id'])
                log[schedule['device_id']][schedule['switch_id']] = switchState[switch_register]
                if int(schedule['oti_status']) == 1 and state == 1:
                    isInterval = True
                if otiState[switch_register]==0 and int(schedule['oti_status']) == 1 and state == 1:
                   otiState[switch_register] = 1
                   printLog("intervals started for : "+schedule['switch_id'])
                   scheduler.scheduleProcess("interval_off_"+schedule['switch_id'],int(schedule['oti_detail']['on_time']),False,instrument,schedule,offSwitch)
                elif otiState[switch_register]==1 and (int(schedule['oti_status']) == 0 or state == 0):
                    otiState[switch_register] = 0
                    writeRegister(schedule['switch_id']+"interval state changed",instrument,switch_register,state)
                    printLog("intervals stoped for : "+schedule['switch_id'])
                    scheduler.clearSchedule("interval_on_"+str(switch_register))
                    scheduler.clearSchedule("interval_off_"+str(switch_register))
        except Exception as e:
                    printLog("\nrouter :------> ** Exception in runProcess main function **")
                    print(e)
    printLog("---------**********************  state before log on server ***************************-----------------")
    printLog(log)
    printLog(keys)
    if len(keys) > 0 :
                try:
                    logData(param,log,keys)
                except Exception as e:
                    print(e)
    constant.setOtiState(otiState)
    constant.setIsInterval(isInterval)

def getRegisterId(schedule):
    return int(schedule['switch_id'].replace(str(constant.getControllerId())+"_SWT", ""))

def onSwitch(instrument,param):
        printInfo("intervalState of "+param['switch_id']+" :------------> ON")
        setSwitch(param,1)
        scheduler.clearSchedule("interval_on_"+param['switch_id'])
        scheduler.scheduleProcess("interval_off_"+param['switch_id'],int(param['oti_detail']['on_time']),False,instrument,param,offSwitch)

def offSwitch(instrument,param):
        printInfo("intervalState of "+param['switch_id']+" :------------> OFF")
        setSwitch(param,0)
        scheduler.clearSchedule("interval_off_"+param['switch_id'])
        scheduler.scheduleProcess("interval_on_"+param['switch_id'],int(param['oti_detail']['off_time']),False,instrument,param,onSwitch)

def setSwitch(param,state):
        intervalState = constant.getIntervalState()
        intervalState[getRegisterId(param)] = state
        constant.setIntervalState(intervalState)

def checkSchedule(schedule):
    printLog("inside checkSchedule function")
    now = datetime.datetime.now().replace(microsecond=0)
    startTime = datetime.datetime.now()
    endTime = datetime.datetime.now()
    start = schedule['start_time'].split(".")
    startTime = startTime.replace(hour=int(start[0]), minute=int(start[1]), second=0, microsecond=0)
    end = schedule['end_time'].split(".")
    endTime = endTime.replace(hour=int(end[0]), minute=int(end[1]), second=0, microsecond=0)
    if startTime < now and now < endTime :
         if int(schedule['phase_type']) == 1 :
             return checkClimate(schedule) # mode 0 : check climate condition and schedule time
         else :
             return 1 # mode 1 : check only schedule time
    else :
        return 0

def checkClimate(schedule):
    printLog("inside checkClimate function")
    climate = constant.getClimate()
    if climate and climate[schedule['device_id']]:
        try:
            param = str(schedule['param'][0])
            onKey = param + "_on"
            offKey = param + "_off"
            printLog('param : '+param)
            printLog('onKey : '+onKey)
            printLog('offKey : '+offKey)
            value = int(climate[schedule['device_id']][param])
            printLog('sensor value : '+str(value))
            if (("less" in schedule[onKey]['mode']) and value < int(schedule[onKey]['value'])) or (("more" in schedule[onKey]['mode']) and value > int(schedule[onKey]['value'])) :
                printLog('inside climate on  : ')
                return 1
            if (("less" in schedule[offKey]['mode']) and value < int(schedule[offKey]['value'])) or (("more" in schedule[offKey]['mode']) and value > int(schedule[offKey]['value'])) :
                printLog('inside climate off  : ')
                return 0
            else :
                return -1
        except Exception as e:
            printLog("Sensor data is not available."+str(e))
            return 0
    else :
        printLog("Sensor data is not available.")
        return 0

def updateClimate(instrument,db,obj):
    printLog("inside updateClimate function")
    climate = constant.getClimate()
    printLog(obj)
    climate[obj["device_id"]] = obj
    printLog(climate)
    try:
        if(obj["status"] and obj["status"]=="1" and (not constant.getIsOnline())):
            now = str(datetime.datetime.utcnow().isoformat() + 'Z')
            obj["timestamp"]= now
            db.logSensorData(json.dumps(obj))
    except Exception as e:
        printLog(e)
    scheduler.clearSchedule(obj["device_id"])
    scheduler.scheduleProcess(obj["device_id"],1,True,instrument, obj["device_id"], clearClimate)

def clearClimate(instrument, param):
    climate = constant.getClimate()
    printLog("inside clearClimate function")
    printLog(climate)
    climate[param] = {}
    printLog(climate)
    scheduler.clearSchedule(param)

def logData(param,log,keys):
    climate = constant.getClimate()
    printLog("-------------------------------------state log on server----------------------------------------")
    printLog(climate)
    data = []
    now = str(datetime.datetime.utcnow().isoformat() + 'Z')
    stringNow = str(now)
    try :
        for i in range(0, len(keys)):
            printLog(keys[i])
            if keys[i] in climate:
                obj = climate[keys[i]]
                obj['switches'] = log[keys[i]]
                obj['automator_id'] = constant.getControllerId()
                obj['timestamp'] = stringNow
                obj['switch_status'] = 1
                printLog(obj)
                data.append(obj)
    except Exception as e:
                    printLog("\n Exception in logData main function **")
                    print(e)
    if len(data)>0 :
        stringData = json.dumps(data).replace(str(constant.getControllerId())+"_SWT", "switch")
        printLog(stringData)
        if not constant.getIsOnline() :
            try:
                param.logOfflineData(stringData)
            except Exception as e:
                print(e)
        else :
            try:
                sio = constant.getSio()
                if sio :
                    sio.emit('request_to_server', {'sensor_data':[],'automator_data':stringData,'cnt_id': str(constant.getControllerId()),'action':"online_data"})
                    printLog("----------------------successs--------------- online_data log on server ----------------------------------------")
            except Exception as e:
                    printLog("\n Exception in logData online function **")
                    print(e)

def checkIntervals(instrument):
    try:
        if constant.getIsInterval():
            intervalState = constant.getIntervalState()
            switchState = constant.getSwitchState()
            currentState = constant.getCurrentState()
            now = datetime.datetime.now().replace(microsecond=0)
            printInfo(now)
            for i in range(1, len(switchState)):
                try:
                    if switchState[i] == 1 and currentState[i]!=intervalState[i]:
                        currentState[i] = intervalState[i]
                        constant.setCurrentState(currentState)
                        writeRegister("-interval switc---> "+str(i),instrument,i,intervalState[i])
                except Exception as e:
                    printLog("\n** Exception in interval switch function **"+str(i))
                    print(e)
    except Exception as e:
            printLog("\n** Exception in checkIntervals function **")
            print(e)

def controllOffline(client,instrument,time):
    job_thread = threading.Thread(target=runOffline, kwargs={"client": client,"instrument":instrument,"time":time})
    job_thread.start()

def runOffline(client,instrument,time):
    while True:
        client.loop_start()
        scheduler.checkPending()
        time.sleep(1)
        checkIntervals(instrument)