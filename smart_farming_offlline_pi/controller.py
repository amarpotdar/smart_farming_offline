import json
import constant
from utility import writeRegister


def printLog(msg):
    print("\ncontroller :----> " + str(msg))

def getId():
    return constant.getControllerId()

def onConnect(instrument,time):
    printLog("Connected to server")
    time.sleep(5)
    if not constant.getIsOnline():
        writeRegister("connected to server",instrument,16,1)
        constant.setIsOnline(True)

def onDataReceived(sio,instrument,db,data):
    if data == "a new user has joined the room":
        printLog("request_to_server:-----------> init_sync")
        sio.emit('request_to_server', {'search_key':"49222_"+str(constant.getControllerId())+"_SWT",'cnt_id': str(constant.getControllerId()),'action':"init_sync"})
    else:
        if not constant.getIsOnline():
            writeRegister("connected to server",instrument,16,1)
            constant.setIsOnline(True)
        try:
            data = data.replace("u'","'")
            obj = json.loads(data)
            printLog("------------------ onDataReceived data convert into Object --------------------------> ")
            printLog(obj)
            if obj["action"] :
                if obj["action"]=="update" or obj["action"]=="init_sync":
                    try :
                        schedules =  [json.loads(json.dumps(i)) for i in obj["data"]]
                        printLog(schedules)
                        if obj["action"]=="init_sync" :
                            db.clearData()
                            logDataToServer(sio,"offline_data",db.getSensorData(),db.getOfflineData())
                            constant.setSio(sio)
                        for i in schedules:
                            db.syncSchedule(i['_id'],json.dumps(i))
                        db.updateSchedule()
                    except Exception as e:
                        printLog("\n ** Exception inside onDataReceived ----> converting schedules string to array of obj ----> data : ")
                        printLog(e)
                elif obj["action"]=="delete":
                    db.deleteSchedule(obj["data"])
                    db.updateSchedule()
                elif obj["action"]=="offline_data":
                    printLog("---------------------------logDataToServer data responce-----------------------------")
                    db.clearOfflineData()
                    db.clearSensorData()
        except Exception:
                printLog("\n ** Exception inside onDataReceived ----> converting schedules string to obj ----> data : ")
                printLog(data)

def disconnect():
    printLog('disconnected from server.......')


def offline(instrument):
    if constant.getIsOnline():
        writeRegister("disconnected from server",instrument,16,0)
        constant.setIsOnline(False)
        constant.setSio({})

def logOnline(sio):
    stringData = constant.getOnlineLog()
    if len(stringData) > 5 :
        logDataToServer(sio,"online_data",[],stringData)
        constant.setOnlineLog("")

def logDataToServer(sio,action,sensor,automator):
    printLog("-------------------------------------logDataToServer----------------------------------------")
    try:
        if len(sensor)>5 or len(automator)>5:
            sio.emit('request_to_server', {'sensor_data':sensor,'automator_data':automator,'cnt_id': str(constant.getControllerId()),'action':action})
    except Exception as e:
        printLog("Exception inside logDataToServer---------------------------------------")
        printLog(e)