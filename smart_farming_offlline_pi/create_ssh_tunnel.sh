#!/bin/bash
sleep 30s
createTunnel() {
  /usr/bin/sshpass -p vyaprut@12345 ssh -N -R 4011:localhost:22 root@www.vyaprut.com
  if ip=$(hostname -I); then
    echo Tunnel to jumpbox created successfully
  else
    echo An error occurred creating a tunnel to jumpbox. RC was $?
  fi
}
/bin/pidof ssh
if ip=$(hostname -I) ; then
  echo Creating new tunnel connection
  createTunnel
fi

# sudo netstat -plant | grep 4011
# sudo netstat -ntlp | grep LISTEN
# ssh -l pi -p 4011 localhost
# sudo apt-get install sshpass
# sudo ufw allow 22/tcp
# */1 * * * * /home/pi/Desktop/smart_shetkari_controller/create_ssh_tunnel.sh
# >/home/pi/logs/sshlog 2>&1
# https://www.tunnelsup.com/raspberry-pi-phoning-home-using-a-reverse-remote-ssh-tunnel/#:~:t
# ext=Creating%20the%20Reverse%20SSH%20tunnel&text=The%20Pi%20is%20ssh'ing,the%20
# Pi%20on%20port%2022.