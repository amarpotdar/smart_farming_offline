import threading
import schedule


def printLog(msg):
    print("\nscheduler :------->->-> " + str(msg))



def scheduleProcess(tagName,time,inMins,instrument,param,callback):
    if inMins :
        printLog("'"+str(tagName)+"' Job scheduled for every "+str(time)+" mins")
        schedule.every(time).minutes.do(run_threaded,callback,instrument,param).tag(tagName)
    else :   
        printLog("'"+str(tagName)+"' Job scheduled for every "+str(time)+" secs") 
        schedule.every(time).seconds.do(run_threaded,callback,instrument,param).tag(tagName)
    return 1

def run_threaded(job_func, instrument,param):
    job_thread = threading.Thread(target=job_func, kwargs={"instrument": instrument,"param":param})
    job_thread.start()


def clearSchedule(tagName):
    try :
        printLog("clear schedule : "+tagName)
        schedule.clear(tagName)
    except Exception as e:
            printLog("** Exception in clearSchedule function **")
            printLog(e)

def clearAllSchedules():
    try :
        schedule.clear()
        printLog("clear all schedules")
    except Exception as e:
            printLog("** Exception in clearAllSchedules function **")
            printLog(e)

def checkPending():
    schedule.run_pending()