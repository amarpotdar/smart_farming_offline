import time
import datetime
import json

def printLog(msg):
    print("\nUtility :----> " + str(msg))

def send(client, action, data):
    printLog("send responce :--> "+str(data))
    try:
        res_action = action + "_Response"
        client.publish("python_producer", json.dumps(
            {'data': data, "type": res_action}, default=myconverter))
    except:
        printLog("** Exception in produceResponse function **")

def writeRegister(title,instrument, register, value):
    try:
        time.sleep(.100)
        instrument.write_register(register, value, 0, 6, False)
        printLog("writeRegister success register :--> "+str(register)+", Title :---> "+title+", value : --> "+str(value))
    except:
        printLog("************ Trying writeRegister ************ register :--> "+str(register)+", Title :---> "+title+", value : --> "+str(value))
        writeRegister(title,instrument, register, value)

def readRegister(instrument, register):
    try:
        data = instrument.read_register(register, 0, 4)
        if data is None:
            printLog("---------------> data is None")
            return 0
        else:
            printLog("read_register : "+str(register)+" , response_value :"+str(data))
            return data
    except:
        printLog("read_register : "+str(register))
        raise Exception("******************** Sorry, read got failed **************")

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()